const path = require("path");
const pretty = require("pretty");

let renderRoutes = require("./src/routerNamesList");

renderRoutes = renderRoutes.filter(item=>item.show).map(item => {
  
    if (item.name === "PageIndex") item.name = "/";
    else item.name = "/" + item.name + "/";
    return item.name;
});
console.log(renderRoutes);

let publicPath = "/";

if (process.env.NODE_ENV === "production" && process.env.VUE_APP_FOR_BACKEND!=='on') {
    publicPath = "/rosgeo/";
}


let object = {
    //indexPath:'./v/index.html',
    //assetsDir:'assets',
    publicPath,
    outputDir: process.env.VUE_APP_FOR_BACKEND=='on' ? "markup" : "dist",
    lintOnSave: false,

    //publicPath:'/dist/',
    configureWebpack: config => {
        if (process.env.NODE_ENV === "production") {
            //config.plugins.push(...productionPlugins);
        }
    },

    filenameHashing: false,
    /*  ignoredElements : [
          'v-banner-nav',
  
      ],*/
    chainWebpack: config => {
        if (process.env.NODE_ENV === "production") {
            //config.plugins.delete("html");

            config.plugins.delete("preload");
            config.plugins.delete("prefetch");
        }

        config.module
            .rule('svg-sprite')
            .use('svgo-loader')
            .loader('svgo-loader')
          /*  .options(
                 {
                    plugins: [
                        { removeViewBox: false,removeDimensions:true,removeTitle:false,addAttributesToSVGElement:true }
                        ]
                }
            )*/

        config.module
            .rule("images")
            .use("url-loader")
            .loader("url-loader")
            .tap(options => Object.assign(options, { limit: 100 }));

        const svgRule = config.module.rule("svg");

        svgRule.uses.clear();

        svgRule
            .oneOf("inline")
            .resourceQuery(/inline/)
            .use("babel-loader")
            .loader("babel-loader")
            .end()
            .use("vue-svg-loader")
            .loader("vue-svg-loader")
          /*  .options({
                svgo: {
                    plugins: [{ removeViewBox: false }]
                }
            })*/
            .end()
            .end()
            .oneOf("external")
            .use("file-loader")
            .loader("file-loader")
            .options({
                name: "assets/[name].[ext]"
            });

        
    },

    css: {
        sourceMap: false,
        loaderOptions: {
            sass: {
                data: '@import "@/scss/settings.scss";'
            }
        }
    }
};

let pluginOptions = {
    prerenderSpa: {},
    svgSprite: {
        /*
         * The directory containing your SVG files.
         */
        dir: "src/assets/svgSprite/icons",
        /*
         * The reqex that will be used for the Webpack rule.
         */
        test: /\.(svg)(\?.*)?$/,
        /*
         * @see https://github.com/kisenka/svg-sprite-loader#configuration
         */
        loaderOptions: {
            extract: process.env.NODE_ENV === "production" && process.env.VUE_APP_FOR_BACKEND=='on'?true:false,
            //runtimeCompat: true,
            spriteFilename: 'img/sprite.svg' // or 'img/icons.svg' if filenameHashing == false
        },
        /*
         * @see https://github.com/kisenka/svg-sprite-loader#configuration
         */
        pluginOptions: {
            plainSprite: true,
           
           
        }
    }
};

object.pluginOptions = pluginOptions;

if (process.env.NODE_ENV === "production" && process.env.VUE_APP_FOR_BACKEND==='on') {
    object.pluginOptions.prerenderSpa = {
        registry: undefined,
        renderRoutes,
        useRenderEvent: true,
        headless: true,
        onlyProduction: true,
        postProcess: route => {
            // Defer scripts and tell Vue it's been server rendered to trigger hydration
            console.log(route.html);
            route.html = pretty(
                route.html
                    .replace(/\.jsOff/g, ".js")
                    //.replace(/<script src="(.*?)js\/main\.js"><\/script>/,'')
                    //.replace(/<script src="(.*?)js\/app\.js"><\/script>/,'')

                    //.replace(/<script (.*?)>/g, '<script $1 defer>')
                    //.replace('class="page"', 'id="app" data-server-rendered="true" class="page"')
                    .replace('class="page"', 'id="app" class="page"')
            );
            return route;
        }
    };
}

module.exports = object;
