import Vue from "vue";
import Router from "vue-router";



import routerNamesList from "@/routerNamesList.js";

let routerList = routerNamesList.map(function(item) {
 

    //.toLocaleLowerCase().split("page")[1];
    var path = item.name 
    if(item.path==='') path=''
    if(item.path) path=item.path
    return {
        path: path,
        name: item.name,
        component: ()=>import('@/pages/'+item.name+'.vue'),
        meta: {
            title: item.title
        }
    };
});

console.log(routerList);

Vue.use(Router);

let publicPath = "/";
let mode = "history";
if (process.env.NODE_ENV === "production" && process.env.VUE_APP_FOR_BACKEND!=='on') {
    publicPath = "/rosgeo/";
    mode = "hash";
}



const router = new Router({
    mode,
    base: publicPath,

    routes: [
        
        ...routerList
        /*    {
              path:'PageCatalog',
              name:'PageCatalog',
              component:PageCatalog,
          }*/
    ]
});

export default router;

router.afterEach((to, from) => {
    setTimeout(() => {
        if(init) {
           
            init();    
        }
        
    }, 1000);
    console.log("afterEach init");
});
