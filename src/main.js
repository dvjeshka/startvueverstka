import Vue from "vue";
import Page from "@/Page";
import router from "@/router";
import store from "@/store";

Vue.component('VNode', ()=>import('@/components/VNode'))

Vue.component('VIcon', ()=>import('@/components/VIcon'))
Vue.component('VTabs', ()=>import('@/components/VTabs'))
Vue.component('TagsStyle', ()=>import('@/components/TagsStyle'))
Vue.component('VContainer', ()=>import('@/components/VContainer'))




/*import(/!* webpackChunkName: "common" *!/ "@/js/common");
import(/!* webpackChunkName: "vendors" *!/ "@/js/vendors");*/

//require('@/js/common')

/*let requireComponent = require.context("@/components", true, /.*\.vue$/);
//console.log('requireComponent.keys()',requireComponent.keys());
requireComponent.keys().forEach(function(fileName) {
    let baseComponentConfig = requireComponent(fileName);
    baseComponentConfig = baseComponentConfig.default || baseComponentConfig;
    let baseComponentName =
        baseComponentConfig.name ||
        fileName.replace(/^.+\//, "").replace(/\.\w+$/, "");
    //console.log(baseComponentName);
    Vue.component(baseComponentName, baseComponentConfig);
});*/

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(Page),
    mounted: () => document.dispatchEvent(new Event("x-app-rendered"))
}).$mount("#app");
