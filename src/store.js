import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        isHeaderFill: false
    },
    mutations: {
        setHeaderFill(state, val) {
            state.isHeaderFill = val;
        }
    },
    actions: {}
});
