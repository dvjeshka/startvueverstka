var init = function(funcName) {

    
    function loadAPIGoggleMapInit()
    {
        
        
        if(!window.initMap) {
            
            function initMap () {

          

               

               
                init('map1')
               

                function init(mapId) {
                    var map = new google.maps.Map(document.getElementById(mapId), {
                        //mapTypeId: google.maps.MapTypeId.ROADMAP,
                        zoom: 3,
                        center: {lat: -28.024, lng: 140.887},
              
                        styles: [
                            {
                                "featureType": "all",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#2b2b2b"
                                    }
                                ]
                            },
                            {
                                "featureType": "all",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "gamma": 0.01
                                    },
                                    {
                                        "lightness": "76"
                                    },
                                    {
                                        "saturation": "16"
                                    },
                                    {
                                        "color": "#cacaca"
                                    }
                                ]
                            },
                            {
                                "featureType": "all",
                                "elementType": "labels.text.stroke",
                                "stylers": [
                                    {
                                        "gamma": "0"
                                    },
                                    {
                                        "lightness": "-100"
                                    },
                                    {
                                        "saturation": "-100"
                                    },
                                    {
                                        "weight": "0.01"
                                    },
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "all",
                                "elementType": "labels.icon",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.country",
                                "elementType": "geometry.stroke",
                                "stylers": [
                                    {
                                        "visibility": "on"
                                    },
                                    {
                                        "color": "#b68800"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.province",
                                "elementType": "geometry.stroke",
                                "stylers": [
                                    {
                                        "visibility": "on"
                                    },
                                    {
                                        "color": "#e4aa00"
                                    }
                                ]
                            },
                            {
                                "featureType": "landscape",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "lightness": 30
                                    },
                                    {
                                        "saturation": "36"
                                    },
                                    {
                                        "color": "#383838"
                                    }
                                ]
                            },
                            {
                                "featureType": "landscape.natural.landcover",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#ff0000"
                                    },
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "landscape.natural.terrain",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "visibility": "on"
                                    },
                                    {
                                        "color": "#2e2e2e"
                                    }
                                ]
                            },
                            {
                                "featureType": "landscape.natural.terrain",
                                "elementType": "geometry.fill",
                                "stylers": [
                                    {
                                        "visibility": "on"
                                    }
                                ]
                            },
                            {
                                "featureType": "landscape.natural.terrain",
                                "elementType": "geometry.stroke",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    },
                                    {
                                        "color": "#ead7d7"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "saturation": 20
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "lightness": "3"
                                    },
                                    {
                                        "saturation": "-100"
                                    },
                                    {
                                        "gamma": "1"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "visibility": "on"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "labels.icon",
                                "stylers": [
                                    {
                                        "visibility": "on"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "visibility": "simplified"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "lightness": 10
                                    },
                                    {
                                        "saturation": -30
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "geometry.stroke",
                                "stylers": [
                                    {
                                        "saturation": 25
                                    },
                                    {
                                        "lightness": 25
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "lightness": -20
                                    },
                                    {
                                        "visibility": "on"
                                    },
                                    {
                                        "color": "#2b2b2b"
                                    },
                                    {
                                        "saturation": "-100"
                                    }
                                ]
                            }
                        ],
                        disableDefaultUI: true
                    });

                    var bounds = new google.maps.LatLngBounds();
                    
                    var markerCluster;

                    function mapSetMarkers(points) {
                        
                        
                        
                        if (markerCluster) {
                            markerCluster.clearMarkers();
                        }
                  
                        var image = {
                            //url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                            url: '/img/marker.svg',
                            //url: 'http://localhost:8080/marker.png',
                            // This marker is 20 pixels wide by 32 pixels high.
                            size: new google.maps.Size(44, 44),
                            // The origin for this image is (0, 0).
                            origin: new google.maps.Point(0, 0),
                            // The anchor for this image is the base of the flagpole at (0, 32).
                            anchor: new google.maps.Point(44,0),
                            //scaledSize: new google.maps.Size(88, 88)

                        };

                        function attachSecretMessage(marker, html) {
                         
                            var infowindow = new google.maps.InfoWindow({
                                content: html
                            });
                            marker.addListener('click', function() {
                                infowindow.open(marker.get('map'), marker);
                            });



                        }
                        var markers = points.map(function (item) {

                            var point = item;

                            var marker = new google.maps.Marker({
                                position: point.position,
                                map: map,
                                icon: image,
                                title: point.title,
                                //zIndex: point[3]

                            });
                            bounds.extend(marker.position);
                            attachSecretMessage(marker,point.html);


                            return marker
                        })
                        
                        
                        

                       

                        markerCluster = new MarkerClusterer(map, markers,
                            {
                                textColor: '#ff00ff',
                                textSize:25,
                                imagePath: '/img/markerGroup/m',
                                clusterClass:'map-marker-group'
                            });


                        var listener = google.maps.event.addListener(map, "idle", function () {
                            map.setZoom(5);
                            google.maps.event.removeListener(listener);
                        });

                        map.fitBounds(bounds);

                        window.mapSetMarkerToggle = function (index,toggle) {
                            console.log(toggle);
                            if(toggle==undefined) {
                                if (!markers[index].visible) markers[index].setVisible(true)
                                else markers[index].setVisible(false)
                            }
                            else {
                                if (toggle) markers[index].setVisible(true)
                                else markers[index].setVisible(false)
                            }
                           
                            markerCluster.setMap(null)
                            markerCluster = new MarkerClusterer(map, 
                                markers.filter(function (item) {return item.visible}),
                                {
                                    textColor: '#ff00ff',
                                    textSize:25,
                                    imagePath: '/img/markerGroup/m',
                                    clusterClass:'map-marker-group'
                                });
                        }
                    }
                    window.mapSetMarkers = mapSetMarkers


                    mapSetMarkers([
                        {title:'Название',position:{lat: -31.563910, lng: 147.154312},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -33.718234, lng: 150.363181},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -33.727111, lng: 150.371124},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -33.848588, lng: 151.209834},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -33.851702, lng: 151.216968},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -34.671264, lng: 150.863657},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -35.304724, lng: 148.662905},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -36.817685, lng: 175.699196},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -36.828611, lng: 175.790222},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -37.750000, lng: 145.116667},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -37.759859, lng: 145.128708},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -37.765015, lng: 145.133858},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -37.770104, lng: 145.143299},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -37.773700, lng: 145.145187},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -37.774785, lng: 145.137978},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -37.819616, lng: 144.968119},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -38.330766, lng: 144.695692},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -39.927193, lng: 175.053218},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -41.330162, lng: 174.865694},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -42.734358, lng: 147.439506},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -42.734358, lng: 147.501315},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -42.735258, lng: 147.438000},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'},
                        {title:'Название',position:{lat: -43.999792, lng: 170.463352},html:'<div class="v-map-marker-popup"><div class="v-map-marker-popup__title">Название проекта</div><div class="v-map-marker-popup__main"><div class="v-map-marker-popup__img"><img src="/img/img44.jpg"></div><div class="v-map-marker-popup__text"><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Выполненные работы</div><div class="v-map-marker-popup__text-content">Бурение с поверхности, горные работы, ГИС</div></div><div class="v-map-marker-popup__text-item"><div class="v-map-marker-popup__text-title">Заказчик</div><div class="v-map-marker-popup__text-content">Алтайрудаметалл</div></div></div></div></div>'}

                    ])
                    
                    
                }


            }


            window.initMap = initMap

            var script = document.createElement("script");

            script.src = "https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js";
            script.type = "text/javascript";
            document.getElementsByTagName("head")[0].appendChild(script);

            script = document.createElement("script");

            script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDcBTpCIXtOojj_RhzQ3ppOkY_4u9OTRH8&callback=initMap&language=ru";
            script.type = "text/javascript";
            document.getElementsByTagName("head")[0].appendChild(script);    
        }
        

    }
    
    
    
    
    
    function sliderInit() {
        $("[data-slick]:not(.slick-initialized)").each(function() {
            var data = $(this).data("slick");

            if (data) {
                if(data.prevArrow && data.nextArrow) {
                    if($(this).closest('.js-slick-arrow-wrap-parent').length) {

                        var prevArrow=$(this).closest('.js-slick-arrow-wrap-parent').find(data.prevArrow)
                        if(prevArrow) data.prevArrow=prevArrow
                        var nextArrow=$(this).closest('.js-slick-arrow-wrap-parent').find(data.nextArrow)
                        if(nextArrow) data.nextArrow=nextArrow
                    }
                }
                var o = {}
                if($(this).has('.v-slider-double-direction__slider-list')) {
                  
                    $.each(data, function (k, v) {

                      
                        o[k] = v;

                    });
                    o['customPaging']=  function(slider, i) {
                       
                        var title = $(slider.$slides[i].children[0]).data('title');
                        return '<a class="pager__item">'+title+'</a>';
                    },
                    console.log(o);
                    data = $.extend(data, o);
                }

                $(this).slick(data);
            } else $(this).slick();
        });
    }
    function popupInit(){
        if ($().colorbox) {



            $('[data-colorbox]').each(function () {

                var s = {

                    previous: '<div class="popup-arrow popup-arrow_left"><span class="v-icon"><svg width="14" height="20" viewBox="0 0 14 20" fill="none" xmlns="http://www.w3.org/2000/svg" class=""><path d="M12.547 18.624L1.304 10.439a.5.5 0 010-.808l11.243-8.186" stroke="#E5B739" stroke-width="1.5" stroke-linecap="round"></path></svg><!----></span></div>',
                    next: '<div class="popup-arrow popup-arrow_right"><span class="v-icon"><!----><svg width="14" height="20" viewBox="0 0 14 20" fill="none" xmlns="http://www.w3.org/2000/svg" class=""><path d="M1.231 1.44l11.244 8.185a.5.5 0 010 .809L1.23 18.619" stroke="#E5B739" stroke-width="1.5" stroke-linecap="round"></path></svg></span></div>',
                    close: '<svg width="19" height="19" viewBox="0 0 19 19"  xmlns="http://www.w3.org/2000/svg">\n' +
                    '<path fill-rule="evenodd" clip-rule="evenodd" d="M8.06252 9.58577L0.162109 17.4869L1.57639 18.9011L9.47667 11L17.377 18.9011L18.7912 17.4869L10.8908 9.58577L18.7902 1.68563L17.3759 0.271484L9.47667 8.17149L1.5774 0.271484L0.163118 1.68563L8.06252 9.58577Z"  />\n' +
                    '</svg>\n',
                    maxWidth: '100%',

                    /*   maxHeight: '100%',*/
                    current: "{current} из {total}",
                    opacity: .9,
                    inline: true,
                    className: 'popup-wrap',
                    /* fixed: true,
                     scrolling: true,*/
                    onOpen: function () {
                        selectedBlock = $(this).data('block');
                    },
                    onClosed: function () {
                        selectedBlock = null;
                    }

                }, o = {};
                $.each($(this).data('colorbox'), function (k, v) {

                    if (k == 'iframe') {
                        o.width = '1600px';
                        o.height = '800px';
                    }
                    o[k] = v;

                });

                s = $.extend(s, o);
                $(this).colorbox(s);
            });
        }
    }

    function tabsInit() {

        
        $('.js-tabs:not(.js-tabs_init)').each(function () {
            var tabs = $(this),
                link = $('[data-tab-target]', tabs);

            $(this).addClass('js-tabs_init')
            
            console.log(link);
            link.on('click', function (e) {
                e.preventDefault()
                var target = $(this).data('tab-target'),
                targetTab = $(this).data('tab-name'),
                tabNameSelector='';
                
                var targetSelector = '[data-tab="' + target + '"]'
                
                if(targetTab) { tabNameSelector = '[data-tab-name="' + targetTab + '"]'}
                
           
                var parent = $(targetSelector+tabNameSelector).closest('.js-tabs')

                if ($(parent).length) {

                    $(parent).find('[data-tab-target]'+tabNameSelector).removeClass('active')
                    $(parent).find('[data-tab]'+tabNameSelector).removeClass('active');

                    $(parent).find(targetSelector+tabNameSelector).addClass('active')
                    $(this).addClass('active');

                }
            });
        });

        /*     var tabs = $('.js-tabs'),
                 btn = $('.js-tabs__header .js-tabs__btn', tabs);
             active($('.js-tabs__header .js-tabs__btn.active', tabs));
             function active(elem) {
                 $(elem).addClass('active').siblings().removeClass('active');
                 $('.js-tabs__contents .js-tabs__content', tabs).siblings().removeClass('active').eq($(elem).index()).addClass('active');
             }
 
             btn.on('click', function () {
                 active(this);
             });*/

    }

    function dataPickerInit() {
    
        if(!$('.js-datapicker-parent').length) return

    
        var picker = new Lightpick({
            parentEl:'.js-datapicker-parent',
            field: document.querySelector('.js-datapicker'),
            singleDate: false,
            onSelect: function(start, end){
                $('.js-datapicker').trigger('change')
            }
        });
        $('.js-datapicker-click-open').click(function (e) {
            e.stopPropagation()
            console.log('e');
            picker.show()

        })

    }


    function projectArticleAboutCustomInit() {
        var parent = $('.js-page-project-article-about-custom');

        parent.find('.project-article-about-custom__close').click(function () {
            parent.find('.project-article-about-custom__item').removeClass('project-article-about-custom__item_show')
        })
        parent.find('.project-article-about-custom__link').click(function () {
            parent.find('.project-article-about-custom__item').removeClass('project-article-about-custom__item_show')
            $(this).closest('.project-article-about-custom__item').toggleClass('project-article-about-custom__item_show')
         

        })
    }


    function complexApproachSecondaryInit() {
        var parent = $('.js-complex-approach-secondary:not(.js-complex-approach-secondary_init)')
        
        if(!$(parent).length) return
        $(parent).addClass('js-complex-approach-secondary_init')
      
        
        
        
        
        
        function scrollEvent() {
            var top = $(this).scrollTop(),
                hH = $('header').height(),
                parentTop = parent.offset().top;

            if(top+300>=parentTop) {
                console.log(parentTop);
                $(window).off('scroll resize',scrollEvent)
                parent.find('.v-content-animate-constructor').removeClass('v-content-animate-constructor_stop-svg')
                setTimeout(function () {
                    parent.find('.v-content-animate-constructor').removeClass('v-content-animate-constructor_together')


                    parent.find('.v-content-animate-constructor__item').hover(function() {
                        $(this).siblings().removeClass('active')
                        $(this).addClass('active');

                        var index = $(this).index()


                        parent.find('.v-content-animate-left-side__img').siblings().removeClass('active')
                        parent.find('.v-content-animate-left-side__img').eq(index).addClass('active')


                        parent.find('.v-content-animate-right-side__item').siblings().removeClass('active')
                        parent.find('.v-content-animate-right-side__item').eq(index).addClass('active')
                    }, function() {
                        $(this).removeClass('active');
                        parent.find('.v-content-animate-left-side__img').siblings().removeClass('active')
                        parent.find('.v-content-animate-right-side__item').siblings().removeClass('active')
                    });

                    parent.find('.v-content-animate-constructor__item').click(function () {
                        parent.addClass('popup-open')
                        parent.find('.v-constructor-banner-default__banners-tabs-item').eq($(this).index()).click()



                        var tab = '[data-tab-target="' + $(this).index() + '"]'
                        parent.find(tab).click()
                    })
                    parent.find('.complex-approach__close').click(function () {
                        parent.removeClass('popup-open')
                    })

                    
                    setTimeout(function () {
                        parent.find('.v-content-animate-constructor__item').eq(0).trigger('mouseenter')    
                    },1000)
                  
                },5000)

            }
        }

        $(window).on('scroll resize',scrollEvent)

      
        

    }

    function selectInit() {
        $('.js-select').each(function () {
            $(this).select2(
                {
                    minimumResultsForSearch: Infinity,
                    dropdownParent:$(this).parent(),
                    width:'element'
                    // allowClear: true
                }
            );
        })

    }

    function rangeInit() {
        /**
         * @param slider HtmlElement with an initialized slider
         * @param threshold Minimum proximity (in percentages) to merge tooltips
         * @param separator String joining tooltips
         */
        function mergeTooltips(slider, threshold, separator) {

            var textIsRtl = getComputedStyle(slider).direction === 'rtl';
            var isRtl = slider.noUiSlider.options.direction === 'rtl';
            var isVertical = slider.noUiSlider.options.orientation === 'vertical';
            var tooltips = slider.noUiSlider.getTooltips();
            var origins = slider.noUiSlider.getOrigins();

            // Move tooltips into the origin element. The default stylesheet handles this.
            tooltips.forEach(function (tooltip, index) {
                if (tooltip) {
                    origins[index].appendChild(tooltip);
                }
            });

            slider.noUiSlider.on('update', function (values, handle, unencoded, tap, positions) {

                var pools = [[]];
                var poolPositions = [[]];
                var poolValues = [[]];
                var atPool = 0;

                // Assign the first tooltip to the first pool, if the tooltip is configured
                if (tooltips[0]) {
                    pools[0][0] = 0;
                    poolPositions[0][0] = positions[0];
                    poolValues[0][0] = values[0];
                }

                for (var i = 1; i < positions.length; i++) {
                    if (!tooltips[i] || (positions[i] - positions[i - 1]) > threshold) {
                        atPool++;
                        pools[atPool] = [];
                        poolValues[atPool] = [];
                        poolPositions[atPool] = [];
                    }

                    if (tooltips[i]) {
                        pools[atPool].push(i);
                        poolValues[atPool].push(values[i]);
                        poolPositions[atPool].push(positions[i]);
                    }
                }

                pools.forEach(function (pool, poolIndex) {
                    var handlesInPool = pool.length;

                    for (var j = 0; j < handlesInPool; j++) {
                        var handleNumber = pool[j];

                        if (j === handlesInPool - 1) {
                            var offset = 0;

                            poolPositions[poolIndex].forEach(function (value) {
                                offset += 1000 - 10 * value;
                            });

                            var direction = isVertical ? 'bottom' : 'right';
                            var last = isRtl ? 0 : handlesInPool - 1;
                            var lastOffset = 1000 - 10 * poolPositions[poolIndex][last];
                            offset = (textIsRtl && !isVertical ? 100 : 0) + (offset / handlesInPool) - lastOffset;

                            // Center this tooltip over the affected handles
                            tooltips[handleNumber].innerHTML = 'RUB ' + '<span class="range-val">' + poolValues[poolIndex].join(separator) + '</span>';
                            tooltips[handleNumber].style.display = 'block';
                            tooltips[handleNumber].style[direction] = offset + '%';
                        } else {
                            // Hide this tooltip
                            tooltips[handleNumber].style.display = 'none';
                        }
                    }
                });
            });
        }
        $('.js-range:not(.js-range_init)').each(function () {
            
            
            
            
            $(this).addClass('js-range_init')
       
            
            var startMin = $(this).data('start-min'),
            startMax = $(this).data('start-max');

            noUiSlider.create($(this)[0], {
                start: [startMin, startMax],
                connect: true,
                tooltips: [true, true],
                update: "input-number",
                range: {
                    'min': 0,
                    'max': 100000000
                }
            });

            $(this)[0].noUiSlider.on('change.one', function () {
                $(this).trigger('change')    
            });

            mergeTooltips($(this)[0], 15, ' - ');
            
        })


    }

    function imgTitleWrapInit() {
        $('.tags-style img').each(function () {
            if($(this).parent().prop("tagName")=='FIGURE') return
            var alt = $(this).attr('alt')
            if(alt) {

                $(this).wrap("<figure></figure>").after("<figcaption>"+alt+"</figcaption>");


            }
        })
    }
    
    
    function pageHistorySectionNavInit() {
        function getOffset(elem) {
            if (elem.getBoundingClientRect) {
                // "правильный" вариант
                return getOffsetRect(elem)
            } else {
                // пусть работает хоть как-то
                return getOffsetSum(elem)
            }
        }

        function getOffsetSum(elem) {
            var top=0, left=0
            while(elem) {
                top = top + parseInt(elem.offsetTop)
                left = left + parseInt(elem.offsetLeft)
                elem = elem.offsetParent
            }

            return {top: top, left: left}
        }

        function getOffsetRect(elem) {
            // (1)
            var box = elem.getBoundingClientRect()

            // (2)
            var body = document.body
            var docElem = document.documentElement

            // (3)
            var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop
            var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft

            // (4)
            var clientTop = docElem.clientTop || body.clientTop || 0
            var clientLeft = docElem.clientLeft || body.clientLeft || 0

            // (5)
            var top = box.top + scrollTop - clientTop
            var left = box.left + scrollLeft - clientLeft

            return { top: Math.round(top), left: Math.round(left) }
        }
        
        
        
        if ($('.js-nav-index-dots:not(.js-nav-index-dots_init)').length) {
            
            
            $('.page-history .v-banner__anchor').click(function () {
                $('.page-history .page-history-section-nav .slick-slide').eq(0).click()
            })
            
            
            
            $('.js-nav-index-dots').addClass('js-nav-index-dots_init')
            var link = $('.js-nav-index-dots [data-target]'),
                indexSections = link.map(function () {
                    var target = $(this).data('target'),
                        anchor = $('#' + target);
                    if (anchor.length) return anchor;
                }),
                lastAnchor;
            link.on('click', function (e,isSlide) {
              
                if(isSlide) return
                var target = $(this).data('target'),
                    hH = $('header').height();
              
                if ($('#' + target).length) {


                    

                    $('html').animate({scrollTop: $('#' + target).offset().top - hH + 1}, 200);
                }
            });
            $(window).on('scroll resize', function () {
                var top = $(this).scrollTop(),
                    hH = $('header').height(),
                    cur = indexSections.map(function () {
                        if ($(this).offset().top <= (top + hH)) return this;
                    });
                cur = cur[cur.length - 1];
                var id = cur && cur.length ? cur[0].id : '';
                if (lastAnchor !== id) {
                    lastAnchor = id;
                    //link.removeClass('dot-current');
                    //$('.js-nav-index-dots').find('[data-target="' + id + '"]').addClass('dot-current');
                    $('.js-nav-index-dots').find('[data-target="' + id + '"]').trigger('click','isSlide')
                }
            });
        }
    }



    // $('.js-v-content-breadcrumbs').find('.v-content-breadcrumbs__item').click(function(event) {
    //     var select = $(this).attr("data-content-select");
    //     $("[data-content-selected = 1]").addClass("working");
    // });
    
    
    function menuInit() {
        $('html').addClass('is-opened-menu')
    }

    


    if(funcName) return this[funcName]

    projectArticleAboutCustomInit();
    complexApproachSecondaryInit()
    popupInit();

    tabsInit();
    dataPickerInit();
    sliderInit();


    loadAPIGoggleMapInit()
    selectInit();
    rangeInit()

    imgTitleWrapInit()
    pageHistorySectionNavInit()
    menuInit()



};

$(document).ready(function() {

    init();


    // The following example creates complex markers to indicate points near
    // Sydney, NSW, Australia. Note that the anchor is set to (0,32) to correspond
    // to the base of the flagpole.





});





// Data for the markers consisting of a name, a LatLng and a zIndex for the
// order in which these markers should display on top of each other.



